#include <memory>
#include <sstream>
#include <iostream>

#include "llvm/Support/raw_os_ostream.h"
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Support/WithColor.h"

// Declares clang::SyntaxOnlyAction.
#include "clang/Frontend/FrontendActions.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
// Declares llvm::cl::extrahelp.
#include "llvm/Support/CommandLine.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/AST/RecursiveASTVisitor.h"

#include "JSONNodeDumper.h"
#include "clang/AST/ASTConsumer.h"

using namespace clang::tooling;
using namespace llvm;
using namespace clang;
using namespace clang::ast_matchers;

// Apply a custom category to all command-line options so that they are the
// only ones displayed.
static llvm::cl::OptionCategory AppCategory("c-to-json options");

// CommonOptionsParser declares HelpMessage with a description of the common
// command-line options related to the compilation database and input files.
// It's nice to have this help message in all tools.
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);

// A help message for this specific tool can be added afterwards.
static cl::extrahelp MoreHelp("\nGenerates a JSON representation of the C source file.\n");

static cl::opt<bool> PrintId("print-id", cl::desc("Print object runtime identifiers."),
                             cl::init(false),
                             cl::cat(AppCategory));

// Output filename option
static cl::opt<std::string> OutputFileName(
    "o", cl::init("-"),
    cl::desc("Output AST in a JSON format to the given file name or '-' for stdout."),
    cl::value_desc("filename"),
    cl::cat(AppCategory));

class JSONPrinter : public ASTConsumer,
                public RecursiveASTVisitor<JSONPrinter> {
  typedef RecursiveASTVisitor<JSONPrinter> base;

public:
  JSONPrinter(raw_ostream &Out)
      : Out(Out)
      {}

  void HandleTranslationUnit(ASTContext &Context) override {
    TranslationUnitDecl *D = Context.getTranslationUnitDecl();
    if (D) {
      auto &Context = D->getASTContext();
      SerializationOptions Opts;
      Opts.PrintId = PrintId.getValue();
      JSONDumper P(Out, Context, Opts);
      P.Visit(D);
    } else {
      base::TraverseDecl(D);
    }
  }

private:
  raw_ostream &Out;
};

struct PrinterFactory {

  PrinterFactory(raw_ostream &Out) : Out(Out)
    {}

  std::unique_ptr<clang::ASTConsumer> newASTConsumer() {
    return llvm::make_unique<JSONPrinter>(Out);
  }

  private:

  raw_ostream &Out;
};

LLVM_ATTRIBUTE_NORETURN static void error(Twine Message) {
  WithColor::error() << Message << '\n';
  exit(1);
}

int main(int argc, const char **argv) {
  CommonOptionsParser OptionsParser(argc, argv, AppCategory);
  ClangTool Tool(OptionsParser.getCompilations(),
                 OptionsParser.getSourcePathList());

  std::error_code EC;
  llvm::ToolOutputFile Out(OutputFileName, EC, llvm::sys::fs::F_Text);
  if (EC)
    error(EC.message());

  PrinterFactory ac(Out.os());
  auto result = Tool.run(newFrontendActionFactory(&ac).get());

  Out.keep();
  return result;
}