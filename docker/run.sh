#!/bin/sh
ROOT=/usr/local
NCPUS=${NCPUS:-1}

# Strip static libraries.
echo "Reducing size of static libraries..."
find "$ROOT/lib" -type f | \
    xargs -t -d '\n' -r -P$NCPUS -n32 try-strip.sh

echo "Reducing size of binaries..."
find "$ROOT/bin" -type f | \
    xargs -d '\n' -r -P$NCPUS -n32 try-strip.sh
