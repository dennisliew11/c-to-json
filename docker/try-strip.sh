#!/bin/bash
file "$@" | sed 's/:  */: /' | grep 'current ar archive' | sed -n -e 's/^\(.*\):[  ]*current ar archive/\1/p' | xargs -t -d '\n' -I\{\} strip -g \{\}
file "$@" | sed 's/:  */: /' | grep ELF | awk -F: '{print $1}' | xargs -t -d '\n' -I\{\} strip -g \{\}
