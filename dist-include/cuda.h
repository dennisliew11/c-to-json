#ifndef CUDA_H

#define CUDA_H

#ifndef size_t
typedef unsigned int size_t;
#endif

extern void __requires(bool);
extern bool __is_pow2(int);
/* ------- */

#define CUDART_PI_F             3.141592654f
#define _DEVICE_QUALIFIER extern
#define __mul24(x,y) ((x)*(y))
#define __umul24(x,y) ((x)*(y))
#define __read(p) true
#define __write(p) true
#define __other_int(p) p
#define __uniform_int(X) ((X) == __other_int(X))
#define __no_read(p) !__read(p)
#define __no_write(p) !__write(p)
#define __implies(e1, e2) (!(e1)||(e2))
#define __read_implies(p, e) __implies(__read(p), e)
#define __write_implies(p, e) __implies(__write(p), e)
#define __write_offset(p) p
#define __read_offset(p) p
#define __same_group true
#define NULL 0

#define __NOP ((void) 1)
#define  __non_temporal_loads_begin()     __NOP
#define  __non_temporal_loads_end()       __NOP
#define  __invariant(X)                   __NOP
#define  __global_invariant(X)            __NOP
#define  __function_wide_invariant(X)     __NOP
#define  __function_wide_candidate_invariant(X)     __NOP
#define  __candidate_invariant(X)         __NOP
#define  __candidate_global_invariant(X)  __NOP
#define  __ensures(X)                     __NOP
#define  __global_ensures(X)              __NOP
#define  __assert                         __requires


extern void __skip_start();
extern void __skip_end();

/* C-1 */
extern double log(double x);
extern double sqrt(double x);
extern double rsqrt(double x);
extern float tanhf(float x);

extern double pow(double x, double y);
extern float rsqrtf(float x);
extern float sqrtf(float x);
extern float exp(float x);
extern float expf(float x);
extern float __expf(float x);
extern float exp2f(float x);
extern float logf(float x);
extern float __logf(float x);
extern float log2f(float x);
extern float log10f(float x);
extern float log1pf(float x);
extern float fabsf(float x);
extern int __ffs(int x);
extern float abs(float x);
extern float sinf(float x);
extern float cosf(float x);
extern float tanf(float x);
extern double sin(double x);
extern double cos(double x);
extern double tan(double x);

extern float atanf(float x);

/* C-2 */
extern float __fdividef(float x, float y);
extern double round(double x);
extern double floor(double x);
extern float ceilf(float x);
extern float floorf(float x);
extern float fminf(float x, float y);

/* cuda_vectors.h */
extern float clamp(float f, float a, float b);

/* INTEGER INTRINSICS */
extern int __clz(int x);


/* ------- */
#define __constant__ __attribute__((annotate("constant")))
#define __device__ __attribute__((annotate("device")))
#define __global__ __attribute__((annotate("global")))
#define __shared__ __attribute__((annotate("shared")))
#define __host__ __attribute__((annotate("host")))

typedef unsigned short ushort;
typedef unsigned int uint;

/* See Table B-1 in CUDA Specification */
/* From vector_functions.h */

#define __MAKE_VECTOR_OPERATIONS(TYPE,NAME) \
  typedef struct {   \
    TYPE x;          \
  } NAME##1;         \
  typedef struct {   \
    TYPE x, y;       \
  } NAME##2;         \
  typedef struct {   \
    TYPE x, y, z;    \
  } NAME##3;         \
  typedef struct {   \
    TYPE x, y, z, w; \
  } NAME##4;         \
  __host__ __device__ static __inline__ NAME##1 make_##NAME##1(TYPE x) \
  { \
    return { x }; \
  } \
  __host__ __device__ static __inline__ NAME##2 make_##NAME##2(TYPE x, TYPE y) \
  { \
    return { x, y }; \
  } \
  __host__ __device__ static __inline__ NAME##2 make_##NAME##2(TYPE x) \
  { \
    return make_##NAME##2(x, x); \
  } \
  __host__ __device__ static __inline__ NAME##3 make_##NAME##3(TYPE x, TYPE y, TYPE z) \
  { \
    return { x, y, z }; \
  } \
  __host__ __device__ static __inline__ NAME##3 make_##NAME##3(TYPE x) \
  { \
    return make_##NAME##3(x, x, x); \
  } \
  __host__ __device__ static __inline__ NAME##4 make_##NAME##4(TYPE x, TYPE y, TYPE z, TYPE w) \
  { \
    return { x, y, z, w }; \
  } \
  __device__ static __inline__ NAME##4 make_##NAME##4(TYPE x) \
  { \
    return make_##NAME##4(x, x, x, x); \
  }

__MAKE_VECTOR_OPERATIONS(signed char,char)
__MAKE_VECTOR_OPERATIONS(unsigned char,uchar)
__MAKE_VECTOR_OPERATIONS(short, short)
__MAKE_VECTOR_OPERATIONS(unsigned short,ushort)
__MAKE_VECTOR_OPERATIONS(int,int)
__MAKE_VECTOR_OPERATIONS(unsigned int,uint)
__MAKE_VECTOR_OPERATIONS(long,long)
__MAKE_VECTOR_OPERATIONS(unsigned long,ulong)
__MAKE_VECTOR_OPERATIONS(long long,longlong)
__MAKE_VECTOR_OPERATIONS(unsigned long long,ulonglong)
__MAKE_VECTOR_OPERATIONS(float,float)
__MAKE_VECTOR_OPERATIONS(double,double)

#undef __MAKE_VECTOR_OPERATIONS

/* from helper_math.h */

#define __MAKE_VECTOR_FROM_SIMILAR(TYPE,ZERO) \
  __host__ __device__ static __inline__ TYPE##2 make_##TYPE##2(TYPE##3 a) \
  { \
    return make_##TYPE##2(a.x, a.y);  /* discards z */ \
  } \
  __host__ __device__ static __inline__ TYPE##3 make_##TYPE##3(TYPE##2 a) \
  { \
    return make_##TYPE##3(a.x, a.y, ZERO); \
  } \
  __host__ __device__ static __inline__ TYPE##3 make_##TYPE##3(TYPE##2 a, TYPE s) \
  { \
    return make_##TYPE##3(a.x, a.y, s); \
  } \
  __host__ __device__ static __inline__ TYPE##4 make_##TYPE##4(TYPE##3 a) \
  { \
    return make_##TYPE##4(a.x, a.y, a.z, ZERO); \
  } \
  __host__ __device__ static __inline__ TYPE##4 make_##TYPE##4(TYPE##3 a, TYPE w) \
  { \
    return make_##TYPE##4(a.x, a.y, a.z, w); \
  }

__MAKE_VECTOR_FROM_SIMILAR(float,0.0f)
__MAKE_VECTOR_FROM_SIMILAR(int,0)
__MAKE_VECTOR_FROM_SIMILAR(uint,0)

#undef __MAKE_VECTOR_FROM_SIMILAR

inline __device__ float4 operator*(float4 b, float a) {
  return make_float4(a*b.x,a*b.y,a*b.z,a*b.w);
}
inline __device__ float4 operator*(float a,float4 b) {
  return make_float4(a*b.x,a*b.y,a*b.z,a*b.w);
}
inline __device__ float4 operator-(float4 a,float4 b) {
  return make_float4(a.x-b.x,a.y-b.y,a.z-b.z,a.w-b.w);
}
inline __device__ float4 operator+(float4 a,float4 b) {
  return make_float4(a.x+b.x,a.y+b.y,a.z+b.z,a.w+b.w);
}
inline __device__ float3 operator/(float3 a,float3 b) {
  return make_float3(a.x/b.x,a.y/b.y,a.z/b.z);
}
inline __device__ float3 operator*(float3 a,float3 b) {
  return make_float3(a.x*b.x,a.y*b.y,a.z*b.z);
}
inline __device__ float3 operator-(float3 a,float3 b) {
  return make_float3(a.x-b.x,a.y-b.y,a.z-b.z);
}
inline __device__ float2 operator*(float a,float2 b) {
  return make_float2(a*b.x,a*b.y);
}
inline __device__ float2 operator+(float2 a,float2 b) {
  return make_float2(a.x+b.x,a.y+b.y);
}
inline __device__ float2 operator-(float2 a,float2 b) {
  return make_float2(a.x-b.x,a.y-b.y);
}

struct __attribute__((device_builtin)) dim3
{
    unsigned int x, y, z;
};
typedef __attribute__((device_builtin)) struct dim3 dim3;

struct __attribute__((device_builtin)) dim4
{
    unsigned int x, y, z, w;
};
typedef __attribute__((device_builtin)) struct dim4 dim4;

uint3 __attribute__((device_builtin)) extern const threadIdx;
uint3 __attribute__((device_builtin)) extern const blockIdx;
dim3 __attribute__((device_builtin)) extern const blockDim;
dim3 __attribute__((device_builtin)) extern const gridDim;
int __attribute__((device_builtin)) extern const warpSize;

extern __attribute__((annotate("device"))) __attribute__((device_builtin)) void __syncthreads(void);
extern __device__ long long atomicAdd(long long* address, int val);
extern __device__ unsigned long long atomicAdd(unsigned long long* address, int val);

#endif /* CUDA_H */
